﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _0505zz.Models
{
    [MetadataType(typeof(UsersMetadata))]
    public partial class Table
    {
    }
    public class UsersMetadata
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="必填栏位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string Name { get; set; }
        [Required(ErrorMessage = "必填栏位")]
        [DisplayName("电子邮件")]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "必填栏位")]
        [DisplayName("密码")]
        [StringLength(10)]
        public string Password { get; set; }
        [Required(ErrorMessage = "必填栏位")]
        [DisplayName("生日")]
        public System.DateTime Birthday { get; set; }
        [Required(ErrorMessage = "必填栏位")]
        [DisplayName("性别")]
        public bool Gender { get; set; }
        
    }
}